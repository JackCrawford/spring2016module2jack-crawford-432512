<!DOCTYPE html>
<head><title>Upload a File</title>
<style type="text/css">

title{
	width: 800px;
	color: white;
	text-align: center;
	background-color: black;
	margin: 0 auto;
	padding: 0;
	font:30px/36px Verdana;
}		
	
body{
	width: 800px;
	color: white;
	background-color: black;
	margin: 0 auto;
	padding: 0;
	font:20px/24px Verdana;
}
</style>
</head>
<p id="title">Upload a File</p>
<?php
session_start(); //do I need to use a session here? Will it delete my data if I want to view the file?
 

$filename = basename($_FILES['uploadedfile']['name']); //Do I get the file from my local directory?
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}
 
$username = $_SESSION['username'];
if( !preg_match('/^[\w_\-]+$/', $username) ){ //Do I need a username?
	echo "Invalid username";
	exit;
}
 
$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename); //Is this my instance?
 
if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
	header("Location: upload_success.html");
	exit;
}else{
	header("Location: upload_failure.html");
	exit;
}
 
?>

<form action = "view.php" method="GET">

    <p>
		<input type="submit" value="Your Files" />
	</p>
    </form>

<?php
if(isset($_GET["files"])){ //Is this correct?
header("Location: home.php");
exit;
}
?>

<form action = "home.php" method="GET">

    <p>
		<input type="submit" value="Home" />
	</p>
    </form>

<?php
if(isset($_GET["Home"])){ //Is this correct?
header("Location: home.php");
exit;
}
?>

<form action = "login.php" method="GET">

    <p>
		<input type="submit" value="Log out" />
	</p>
    </form>

<?php
if(isset($_GET["Log out"])){ //Is this correct?
header("Location: login.php");
exit;
}
?>

</body>
</html>