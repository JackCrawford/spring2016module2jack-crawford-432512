<!DOCTYPE html>
<head><title>Delete a File</title>
<style type="text/css">

title{
	width: 800px;
	color: white;
	text-align: center;
	background-color: black;
	margin: 0 auto;
	padding: 0;
	font:30px/36px Verdana;
}		
	
body{
	width: 800px;
	color: white;
	background-color: black;
	margin: 0 auto;
	padding: 0;
	font:20px/24px Verdana;
}
</style>
</head>
<p id="title">Login</p>
<body>

<form enctype="multipart/form-data" action="uploader.php" method="POST">
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	</p>
	<p>
		<input type="submit" value="Upload File" />
	</p>
</form> //what kind of input should I have for the delete option?

<?php
session_start(); //will the delete be permanent even though I have a session?
$file = "delfile";
if (!unlink($file))
  {
  echo ("Error deleting $file");
  }
else
  {
  echo ("Deleted $file");
  }
?> //Used code from http://www.w3schools.com/php/func_filesystem_unlink.asp

<form action = "info.php" method="GET">

    <p>
		<input type="submit" value="Home" />
	</p>
    </form>

<?php
if(isset($_GET["Home"])){ //Is this correct?
header("Location: home.php");
exit;
}
?>

<form action = "info.php" method="GET">

    <p>
		<input type="submit" value="Log out" />
	</p>
    </form>

<?php
if(isset($_GET["Log out"])){ //Is this correct?
header("Location: login.php");
exit;
}
?>

</body>
</html>