<!DOCTYPE html>
<head><title>View a File</title>
<style type="text/css">

title{
	width: 800px;
	color: white;
	text-align: center;
	background-color: black;
	margin: 0 auto;
	padding: 0;
	font:30px/36px Verdana;
}		

body{
	width: 800px;
	color: white;
	background-color: black;
	margin: 0 auto;
	padding: 0;
	font:20px/24px Verdana;
}
</style>
</head>
<p id="title">Login</p>
<body>
	
<form enctype="multipart/form-data" action="uploader.php" method="POST"> //Is this the right setup for viewing a document?
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="viewfile_input">Select a file to view:</label> <input name="viewedfile" type="file" id="viewfile_input" />
	</p>
	<p>
		<input type="submit" value="View File" />
	</p>
</form> //**should be a document from uploaded documents**

<?php
session_start();
$filename = $_GET['viewedfile']; //is this the right input?
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}

$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($full_path);

header("Content-Type: ".$mime);
readfile($full_path);
?>

<form action = "info.php" method="GET">

    <p>
		<input type="submit" value="Home" />
	</p>
    </form>

<?php
if(isset($_GET["Home"])){ //Is this correct?
header("Location: home.php");
exit;
}
?>

<form action = "info.php" method="GET">

    <p>
		<input type="submit" value="Log out" />
	</p>
    </form>

<?php
if(isset($_GET["Log out"])){ //Is this correct?
header("Location: login.php");
exit;
}
?>

</body>
</html>