<!DOCTYPE html>
<head><title>Public Forum</title>
<style type="text/css">

title{
	width: 800px;
	color: darkgray;
	text-align: center;
	background-color: white;
	margin: 0 auto;
	padding: 0;
	font:30px/36px Verdana;
}	

body{
	width: 800px;
	color: darkgray;
	background-color: white;
	margin: 0 auto;
	padding: 0;
	font:20px/24px Verdana;
}
</style>
</head>
<p id="title">Public Forum</p>
<body>
	
<?php
session_start(); //do I need to use a session here? Will it delete my data if I want to view the file?
 

$filename = basename($_FILES['uploadedfile']['name']); //Do I get the file from my local directory?
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}
 
$username = $_SESSION['username'];
if( !preg_match('/^[\w_\-]+$/', $username) ){ //Do I need a username?
	echo "Invalid username";
	exit;
}
 
$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename); //Is this my instance?
 
if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
	header("Location: upload_success.html");
	exit;
}else{
	header("Location: upload_failure.html");
	exit;
}
 
?>

<form enctype="multipart/form-data" action="uploader.php" method="POST"> //Is this the right setup for viewing a document?
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="viewfile_input">Select a file to view:</label> <input name="viewedfile" type="file" id="viewfile_input" />
	</p>
	<p>
		<input type="submit" value="View File" />
	</p>
</form> //**should be a document from uploaded documents**

<?php
session_start();
$filename = $_GET['viewedfile']; //is this the right input?
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}

$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($full_path);

header("Content-Type: ".$mime);
readfile($full_path);
?>

<?php
$conn_id = ftp_connect($ftp_server); //need to know if a) this is the correct format, and b) which server and conn_id

$contents = ftp_nlist($conn_id, ".");

var_dump($contents); //Derived code from http://php.net/manual/en/function.ftp-nlist.php
?>

<form action = "info.php" method="GET"> //What do I need instead of info.php? (Can apply to other pages)

    <p>
		<input type="submit" value="Home" />
	</p>
    </form>

<?php
if(isset($_GET["Home"])){ //Is this correct?
header("Location: home.php");
exit;
}
?>

<form action = "info.php" method="GET">

    <p>
		<input type="submit" value="Back to login" />
	</p>
    </form>

<?php
if(isset($_GET["Back to login"])){ //Is this correct?
header("Location: login.php");
exit;
}
?>

</body>
</html>